﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using TheBotAPI.Models;

namespace TheBotAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BotsController : ControllerBase
    {
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public ActionResult<IEnumerable<Bot>> Get()
        {
            using (var session = DocumentStoreHolder.Store.OpenSession())
            {
                return Ok(session.Query<Bot>().ToList());
            }
        }

        [HttpGet("{id}", Name = nameof(GetBot))]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(string), StatusCodes.Status404NotFound)]
        public ActionResult<Bot> GetBot(string id)
        {
            using (var session = DocumentStoreHolder.Store.OpenSession())
            {
                var bot = session.Query<Bot>().FirstOrDefault(b => b.Id == id);

                if (bot == null)
                {
                    return NotFound("Bot not found");
                }

                return Ok(bot);
            }
        }

        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(typeof(string), StatusCodes.Status400BadRequest)]
        public ActionResult<Bot> Post([FromBody] Bot bot)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            using (var session = DocumentStoreHolder.Store.OpenSession())
            {
                var botFound = session.Query<Bot>().FirstOrDefault(b => b.Id == bot.Id);

                if (botFound != null)
                {
                    return BadRequest("Bot with the given ID already exists");
                }

                session.Store(bot);
                session.SaveChanges();
            }

            return CreatedAtAction(nameof(GetBot), new { bot.Id }, bot);
        }

        [HttpPut("{id}")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(typeof(string), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(string), StatusCodes.Status404NotFound)]
        [ProducesDefaultResponseType]
        public ActionResult<Bot> Put(string id, [FromBody] Bot bot)
        {
            if (id != bot.Id)
            {
                return BadRequest("Bot Id is not valid");
            }

            using (var session = DocumentStoreHolder.Store.OpenSession())
            {
                var botFound = session.Query<Bot>().FirstOrDefault(b => b.Id == id);

                if (botFound == null)
                {
                    return NotFound("Bot not found");
                }

                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }

                botFound.Name = bot.Name;
                session.SaveChanges();

                return CreatedAtAction(nameof(Get), new { id }, bot);
            }
        }

        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(string), StatusCodes.Status404NotFound)]
        public ActionResult<Bot> Delete(string id)
        {
            using (var session = DocumentStoreHolder.Store.OpenSession())
            {
                var bot = session.Query<Bot>().FirstOrDefault(b => b.Id == id);

                if (bot == null)
                {
                    return NotFound("Bot not found");
                }

                session.Delete(bot.Id);
                session.SaveChanges();

                return Ok(bot);
            }
        }
    }
}
