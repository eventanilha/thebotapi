﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;

namespace TheBotAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MessagesController : ControllerBase
    {
        [HttpGet("{id}", Name = nameof(GetMessage))]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(string), StatusCodes.Status404NotFound)]
        public ActionResult<Models.Message> GetMessage(string id)
        {
            using (var session = DocumentStoreHolder.Store.OpenSession())
            {
                var message = session.Query<Models.Message>().FirstOrDefault(m => m.Id == id);

                if (message == null)
                {
                    return NotFound("Message not found");
                }

                return Ok(message);
            }
        }

        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(string), StatusCodes.Status404NotFound)]
        public ActionResult<IEnumerable<Models.Message>> Get(string conversationId)
        {
            using (var session = DocumentStoreHolder.Store.OpenSession())
            {
                var messages = session.Query<Models.Message>().Where(m => m.ConversationId == conversationId).ToList();

                if (messages.Count == 0)
                {
                    return NotFound("Conversation not found");
                }

                return Ok(messages);
            }
        }

        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(typeof(string), StatusCodes.Status400BadRequest)]
        public ActionResult<Models.Message> Post([FromBody] Models.Message message)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            using (var session = DocumentStoreHolder.Store.OpenSession())
            {
                var messageFound = session.Query<Models.Message>().FirstOrDefault(m => m.Id == message.Id);

                if (messageFound != null)
                {
                    return BadRequest("Message with the given ID already exists");
                }

                session.Store(message);
                session.SaveChanges();
            }

            return CreatedAtAction(nameof(GetMessage), new { message.Id }, message);
        }
    }
}
