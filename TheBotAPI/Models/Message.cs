﻿using System;
using System.ComponentModel.DataAnnotations;

namespace TheBotAPI.Models
{
    public class Message
    {
        [Required]
        public string Id { get; set; }

        [Required]
        public string ConversationId { get; set; }

        [Required]
        public string Timestamp { get; set; }

        [Required]
        public string From { get; set; }

        [Required]
        public string To { get; set; }

        [Required]
        public string Text { get; set; }

    }
}
