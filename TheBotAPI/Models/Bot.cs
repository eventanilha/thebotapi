﻿using System.ComponentModel.DataAnnotations;

namespace TheBotAPI.Models
{
    public class Bot
    {
        [Required]
        public string Id { get; set; }

        [Required]
        public string Name { get; set; }
    }
}
